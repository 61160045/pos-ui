/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.poscoffee;

import java.util.ArrayList;

/**
 *
 * @author watan
 */
public class Product {
    private int id;
    private String name;
    private double price;
    private String image;

    public Product(int id, String name, double price, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public String getImage() {
        return image;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", image=" + image + '}';
    }
    
    public static ArrayList<Product> genProductList() {
        ArrayList<Product> list = new ArrayList<>();
        list.add(new Product(1, "Espresso",40,"Espresso1.jpg"));
        list.add(new Product(1, "Cocoa",45,"Cocoa2.jpg"));
        list.add(new Product(1, "Americano",50,"Americano3.jpg"));
        list.add(new Product(1, "Latte",40,"1.jpg"));
        list.add(new Product(1, "Mocca",55,"1.jpg"));
        list.add(new Product(1, "Caramel",30,"1.jpg"));
        list.add(new Product(1, "Black Coffee",40,"1.jpg"));
        return list;
    }
    
}
